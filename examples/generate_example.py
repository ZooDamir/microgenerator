import pathlib
import json
from MicroGenerator.MicroGenerator import MicroGenerator
from service_description_model import ServiceDescription, BusinessLogicDescription, \
    WebDescription, AppDescription, BusinessEntityDescription

with open('service_description.json') as f:
    description = json.load(f)
    description = ServiceDescription(
        description['service_name'],
        BusinessLogicDescription(
            [
                BusinessEntityDescription(entity['name'], entity['generates_exceptions'])
                for entity in description['business_logic']['entities']
            ],
            description['business_logic']['utils'],
        ),
        WebDescription(
            [
                AppDescription(
                    app['business_entity'],
                    app['generate_model'],
                    app['generate_view'],
                )
                for app in description['web']['apps']
            ],
        )
    )

generator = MicroGenerator(pathlib.Path('D://Test'), description)  # D://Test
generator.generate_service()
