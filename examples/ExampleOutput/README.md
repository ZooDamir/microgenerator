# TestService


## Description
*Provide service description here*.


## Entities

### E1

*Brief description of the entity. What it is used for*.

#### Fields
| Name | Type | Description |
|------|------|-------------|
|      |      |             |

#### Methods
| Name | Parameter | Output | Description |
|------|-----------|--------|-------------|
|      |           |        |             |


### E2

*Brief description of the entity. What it is used for*.

#### Fields
| Name | Type | Description |
|------|------|-------------|
|      |      |             |

#### Methods
| Name | Parameter | Output | Description |
|------|-----------|--------|-------------|
|      |           |        |             |


### E3

*Brief description of the entity. What it is used for*.

#### Fields
| Name | Type | Description |
|------|------|-------------|
|      |      |             |

#### Methods
| Name | Parameter | Output | Description |
|------|-----------|--------|-------------|
|      |           |        |             |


### E4

*Brief description of the entity. What it is used for*.

#### Fields
| Name | Type | Description |
|------|------|-------------|
|      |      |             |

#### Methods
| Name | Parameter | Output | Description |
|------|-----------|--------|-------------|
|      |           |        |             |


### E5

*Brief description of the entity. What it is used for*.

#### Fields
| Name | Type | Description |
|------|------|-------------|
|      |      |             |

#### Methods
| Name | Parameter | Output | Description |
|------|-----------|--------|-------------|
|      |           |        |             |


## Communication

Describes different means of communication with service. Ex. web/mq etc.

### Web

Describes web apps. Includes API and models (if exist).

#### E1App

##### Model
| Name | Type |
|------|------|
|      |      |

##### API
| Method | URL | Query params | Body params | Description |
|--------|-----|--------------|-------------|-------------|
|        |     |              |             |             |


#### E2App

##### Model
| Name | Type |
|------|------|
|      |      |



#### E3App

##### API
| Method | URL | Query params | Body params | Description |
|--------|-----|--------------|-------------|-------------|
|        |     |              |             |             |


#### E4App

##### Model
| Name | Type |
|------|------|
|      |      |

##### API
| Method | URL | Query params | Body params | Description |
|--------|-----|--------------|-------------|-------------|
|        |     |              |             |             |

