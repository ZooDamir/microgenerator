import aiohttp.web as web
from Web.TestService.TestServiceBuilder import TestServiceAppBuilder
from Web.TestService.urls import urls


builder = TestServiceAppBuilder()
builder.add_routes(urls)

app = builder.get_app()
web.run_app(app, port=8080)
