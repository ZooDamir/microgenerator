import dataclasses
import typing
from Web.TestService.models import DataAccessor


@dataclasses.dataclass()
class E2Model:
    pass


class E2Accessor(DataAccessor):
    @staticmethod
    def _convert_row_to_model_(row_to_convert_from: tuple) -> E2Model:
        """
        Converts selected db row to model object.
        """
        # ToDo implement your conversion logic here
        # output = E2Model(*row_to_convert_from)

        # return output
        pass

    def get_e2_by_id(self, e2_id) -> typing.Optional[E2Model]:
        query = """SELECT ... FROM ... WHERE ... = ?;"""
        cursor = self._select_connection_.cursor()
        data = cursor.execute(query, (e2_id, )).fetchone()
        cursor.close()
        output = None

        if data:
            output = self._convert_row_to_model_(data)
        return output

    def get_all_e2s(self) -> list[E2Model]:
        query = """SELECT ... FROM ...;"""
        cursor = self._select_connection_.cursor()
        data = cursor.execute(query).fetchall()
        cursor.close()
        output = []

        if data:
            output = [self._convert_row_to_model_(row) for row in data]

        return output

    def delete_e2(self, e2_id):
        query = """DELETE FROM ... WHERE ... = ?;"""
        cursor = self._delete_connection_.cursor()
        cursor.execute(query, (e2_id, ))
        self._delete_connection_.commit()
        cursor.close()

        return

    def insert_e2(self, e2):
        # Generating query that can work only in simple cases, so consider rewriting it. As a bonus you get a bit of performance
        values_to_insert = dataclasses.astuple(e2)
        param_subs = ("?, " * len(values_to_insert))[:-2]
        query = f"""INSERT INTO ... VALUES ({param_subs});"""
        cursor = self._insert_connection_.cursor()
        cursor.execute(query, values_to_insert)
        self._insert_connection_ .commit()
        cursor.close()

        return
