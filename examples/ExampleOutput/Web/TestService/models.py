import abc
import sqlite3


class DataAccessor(abc.ABC):
    def __init__(self, db_path: str):
        self._select_connection_ = sqlite3.connect(db_path)
        self._insert_connection_ = sqlite3.connect(db_path)
        self._delete_connection_ = sqlite3.connect(db_path)
        self._update_connection_ = sqlite3.connect(db_path)
