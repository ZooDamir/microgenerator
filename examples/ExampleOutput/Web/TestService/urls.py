from aiohttp.web import View
import Web.E1App.urls
import Web.E3App.urls
import Web.E4App.urls


urls = []


def add_with_prefix(prefix: str, urls_to_add: list[(str, View)], all_urls: list[(str, View)]):
    all_urls.extend(((prefix+x[0], x[1]) for x in urls_to_add))


add_with_prefix('/e1s', Web.E1App.urls.urls, urls)
add_with_prefix('/e3s', Web.E3App.urls.urls, urls)
add_with_prefix('/e4s', Web.E4App.urls.urls, urls)
