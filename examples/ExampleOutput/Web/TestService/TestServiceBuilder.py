import aiohttp.web as web
from Web.E1App.model import E1Accessor
from Web.E2App.model import E2Accessor
from Web.E4App.model import E4Accessor


class TestServiceAppBuilder:
    def __init__(self):
        self._app_ = web.Application()
        self._db_path_ = 'SOME PATH'

    def add_routes(self, routes: [str, web.View]):
        for path, view in routes:
            self._app_.router.add_view(path, view)

    def add_startup_handler(self, handler):
        self._app_.on_startup.append(handler)

    def get_app(self):
        self._app_['e1_accessor'] = E1Accessor(self._db_path_)
        self._app_['e2_accessor'] = E2Accessor(self._db_path_)
        self._app_['e4_accessor'] = E4Accessor(self._db_path_)

        return self._app_
