from Web.E3App.views import E3View, E3sView

urls = [
    ('/', E3sView),
    ('/{e3_id}/', E3View),
]
