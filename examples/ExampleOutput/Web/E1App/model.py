import dataclasses
import typing
from Web.TestService.models import DataAccessor


@dataclasses.dataclass()
class E1Model:
    pass


class E1Accessor(DataAccessor):
    @staticmethod
    def _convert_row_to_model_(row_to_convert_from: tuple) -> E1Model:
        """
        Converts selected db row to model object.
        """
        # ToDo implement your conversion logic here
        # output = E1Model(*row_to_convert_from)

        # return output
        pass

    def get_e1_by_id(self, e1_id) -> typing.Optional[E1Model]:
        query = """SELECT ... FROM ... WHERE ... = ?;"""
        cursor = self._select_connection_.cursor()
        data = cursor.execute(query, (e1_id, )).fetchone()
        cursor.close()
        output = None

        if data:
            output = self._convert_row_to_model_(data)
        return output

    def get_all_e1s(self) -> list[E1Model]:
        query = """SELECT ... FROM ...;"""
        cursor = self._select_connection_.cursor()
        data = cursor.execute(query).fetchall()
        cursor.close()
        output = []

        if data:
            output = [self._convert_row_to_model_(row) for row in data]

        return output

    def delete_e1(self, e1_id):
        query = """DELETE FROM ... WHERE ... = ?;"""
        cursor = self._delete_connection_.cursor()
        cursor.execute(query, (e1_id, ))
        self._delete_connection_.commit()
        cursor.close()

        return

    def insert_e1(self, e1):
        # Generating query that can work only in simple cases, so consider rewriting it. As a bonus you get a bit of performance
        values_to_insert = dataclasses.astuple(e1)
        param_subs = ("?, " * len(values_to_insert))[:-2]
        query = f"""INSERT INTO ... VALUES ({param_subs});"""
        cursor = self._insert_connection_.cursor()
        cursor.execute(query, values_to_insert)
        self._insert_connection_ .commit()
        cursor.close()

        return
