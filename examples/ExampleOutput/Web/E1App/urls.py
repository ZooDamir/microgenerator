from Web.E1App.views import E1View, E1sView

urls = [
    ('/', E1sView),
    ('/{e1_id}/', E1View),
]
