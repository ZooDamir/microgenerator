from Web.E4App.views import E4View, E4sView

urls = [
    ('/', E4sView),
    ('/{e4_id}/', E4View),
]
