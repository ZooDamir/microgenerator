import dataclasses
import typing
from Web.TestService.models import DataAccessor


@dataclasses.dataclass()
class E4Model:
    pass


class E4Accessor(DataAccessor):
    @staticmethod
    def _convert_row_to_model_(row_to_convert_from: tuple) -> E4Model:
        """
        Converts selected db row to model object.
        """
        # ToDo implement your conversion logic here
        # output = E4Model(*row_to_convert_from)

        # return output
        pass

    def get_e4_by_id(self, e4_id) -> typing.Optional[E4Model]:
        query = """SELECT ... FROM ... WHERE ... = ?;"""
        cursor = self._select_connection_.cursor()
        data = cursor.execute(query, (e4_id, )).fetchone()
        cursor.close()
        output = None

        if data:
            output = self._convert_row_to_model_(data)
        return output

    def get_all_e4s(self) -> list[E4Model]:
        query = """SELECT ... FROM ...;"""
        cursor = self._select_connection_.cursor()
        data = cursor.execute(query).fetchall()
        cursor.close()
        output = []

        if data:
            output = [self._convert_row_to_model_(row) for row in data]

        return output

    def delete_e4(self, e4_id):
        query = """DELETE FROM ... WHERE ... = ?;"""
        cursor = self._delete_connection_.cursor()
        cursor.execute(query, (e4_id, ))
        self._delete_connection_.commit()
        cursor.close()

        return

    def insert_e4(self, e4):
        # Generating query that can work only in simple cases, so consider rewriting it. As a bonus you get a bit of performance
        values_to_insert = dataclasses.astuple(e4)
        param_subs = ("?, " * len(values_to_insert))[:-2]
        query = f"""INSERT INTO ... VALUES ({param_subs});"""
        cursor = self._insert_connection_.cursor()
        cursor.execute(query, values_to_insert)
        self._insert_connection_ .commit()
        cursor.close()

        return
