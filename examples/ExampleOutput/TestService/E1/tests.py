import unittest
from TestService.E1.e1 import E1
from TestService.E1.exceptions import E1InvalidInitException, E1ValueOutOfBoundsException


class E1TestCase(unittest.TestCase):
    def setUp(self) -> None:
        self.entity = E1()  # ToDo add init of entity

    def test_e1_raises_invalidinit(self):
        pass

    def test_e1_raises_valueoutofbounds(self):
        pass
