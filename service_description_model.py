import dataclasses


@dataclasses.dataclass(slots=True)
class BusinessEntityDescription:
    name: str
    generates_exceptions: list[str]


@dataclasses.dataclass(slots=True)
class BusinessLogicDescription:
    entities: list[BusinessEntityDescription]
    utils: list[str]


@dataclasses.dataclass(slots=True)
class AppDescription:
    business_entity: str
    generate_model: bool
    generate_view: bool


@dataclasses.dataclass(slots=True)
class WebDescription:
    apps_description: list[AppDescription]


@dataclasses.dataclass(slots=True)
class ServiceDescription:
    service_name: str
    business_logic_description: BusinessLogicDescription
    web_description: WebDescription
