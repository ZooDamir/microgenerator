import pathlib
from service_description_model import ServiceDescription
import MicroGenerator.tokens as tokens


class MdGenerator:
    def __init__(self, base_dir: pathlib.Path, service_description: ServiceDescription):
        self._service_description_ = service_description
        self._base_dir_ = base_dir

    @staticmethod
    def _generate_description_() -> str:
        output = f'## Description\n' \
                 f'*Provide service description here*.\n'

        return output

    @staticmethod
    def _generate_table_(headers: list[str], data: list[str]) -> str:
        """
        Generates md table string.

        :param headers: Headers of the table
        :param data: Rows of the table. For now, it is **UNUSED**.
        :return: Correct md table with set columns and *in the future* rows.
        """
        formatted_headers = [f'| {header} ' for header in headers]
        formatted_headers[-1] += '|'

        header_line = ''.join(formatted_headers)
        align_line = ''.join(f'|{"-"*(len(header) - (1 + (i == len(formatted_headers) - 1)))}'
                             for i, header in enumerate(formatted_headers))
        align_line += '|'
        data_lines = ''.join(f'|{" "*(len(header) - (1 + (i == len(formatted_headers) - 1)))}'
                             for i, header in enumerate(formatted_headers))
        data_lines += '|'

        return f'{header_line}\n' \
               f'{align_line}\n' \
               f'{data_lines}\n'

    def _generate_entities_description_(self) -> str:
        entities_description = '\n\n'.join(f'### {entity.name.capitalize()}\n'
                                           f'\n'
                                           f'*Brief description of the entity. What it is used for*.\n'
                                           f'\n'
                                           f'#### Fields\n'
                                           f'{self._generate_table_(["Name", "Type", "Description"], [])}'
                                           f'\n'
                                           f'#### Methods\n'
                                           f'{self._generate_table_(["Name", "Parameter", "Output", "Description"], [])}'
                                           for entity in self._service_description_.business_logic_description.entities)

        output = f'## Entities\n' \
                 f'\n' \
                 f'{entities_description}'

        return output

    def _generate_web_model_description_(self) -> str:
        output = f'##### Model\n' \
                 f'{self._generate_table_(["Name", "Type"], [])}\n'

        return output

    def _generate_api_description_(self) -> str:
        output = f'##### API\n' \
                 f'{self._generate_table_(["Method", "URL", "Query params", "Body params", "Description"], [])}'

        return output

    def _generate_communication_description_(self) -> str:
        apps_description = '\n\n'.join(f'#### {app.business_entity.capitalize()}App\n'
                                       f'\n'
                                       f'{self._generate_web_model_description_() if app.generate_model else ""}'
                                       f'{self._generate_api_description_() if app.generate_view else ""}'
                                       for app in self._service_description_.web_description.apps_description)
        web_comm_description = f'### Web\n' \
                               f'\n' \
                               f'Describes web apps. Includes API and models (if exist).\n' \
                               f'\n' \
                               f'{apps_description}\n'

        output = f'## Communication\n' \
                 f'\n' \
                 f'Describes different means of communication with service. Ex. web/mq etc.\n' \
                 f'\n' \
                 f'{web_comm_description}'

        return output

    def generate(self):
        with open(self._base_dir_.joinpath('README.md'), 'w') as f:
            readme = f'# {self._service_description_.service_name}\n' \
                     f'\n\n' \
                     f'{self._generate_description_()}' \
                     f'\n\n' \
                     f'{self._generate_entities_description_()}' \
                     f'\n\n' \
                     f'{self._generate_communication_description_()}'

            f.write(readme)
