import pathlib
import os
from service_description_model import ServiceDescription
from MicroGenerator.BusinessPartGenerator import BusinessPartGenerator
from MicroGenerator.CommunicationGenerators.WebPartGenerator import WebPartGenerator
from MicroGenerator.DocsGenerators.MdGenerator import MdGenerator


class MicroGenerator:
    def __init__(self, base_dir: pathlib.Path, service_description: ServiceDescription, python_version: int = 9):
        self._base_dir_ = base_dir
        self._service_description_ = service_description

        self._business_generator_ = BusinessPartGenerator(base_dir, service_description.service_name,
                                                          service_description.business_logic_description)
        self._web_generator_ = WebPartGenerator(base_dir, service_description.service_name,
                                                service_description.web_description, python_version)
        self._docs_generator_ = MdGenerator(base_dir, service_description)

    @staticmethod
    def _generate_python_module_(base_dir: pathlib.Path, module_name: str) -> pathlib.Path:
        module_dir_path = base_dir.joinpath(module_name)
        os.mkdir(module_dir_path)
        open(module_dir_path.joinpath('__init__.py'), 'w').close()

        return module_dir_path

    def _generate_business_part_(self) -> None:
        """
        Generates structure for business part of service.

        :return:
        """
        self._business_generator_.generate_utils()
        self._business_generator_.generate_business_entities()

    def _generate_web_part_(self) -> None:
        """
        Generates structure for web part of service.

        :return:
        """
        self._web_generator_.generate_web_apps()
        self._web_generator_.generate_project_app()
        self._web_generator_.generate_web_main()

    def _generate_readme_(self):
        self._docs_generator_.generate()

    def _generate_requirements_(self):
        with open(self._base_dir_.joinpath('requirements.txt'), 'w') as f:
            reqs = 'aiohttp==3.8.4\n' \
                   'aiosignal==1.3.1\n' \
                   'async-timeout==4.0.2\n' \
                   'attrs==22.2.0\n' \
                   'charset-normalizer==3.0.1\n' \
                   'frozenlist==1.3.3\n' \
                   'multidict==6.0.4\n' \
                   'yarl==1.8.2\n'

            f.write(reqs)

    def generate_service(self) -> None:
        """
        Generates service.

        :return:
        """
        self._generate_business_part_()
        self._generate_web_part_()
        self._generate_readme_()
        self._generate_requirements_()
