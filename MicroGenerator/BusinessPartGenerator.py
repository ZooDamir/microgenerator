import io
import pathlib
import MicroGenerator.tokens as tokens
from service_description_model import BusinessLogicDescription, BusinessEntityDescription
from MicroGenerator.PartGenerator import PartGenerator
from MicroGenerator.Templates.ClassTemplate import ClassTemplate


class BusinessPartGenerator(PartGenerator):
    def __init__(self, base_dir: pathlib.Path, service_name: str, business_description: BusinessLogicDescription):
        self._service_name_ = service_name
        self._business_dir_path_ = self._generate_python_module_(base_dir, service_name)
        self._description_ = business_description

    def generate_utils(self) -> None:
        """
        Generates utils folder structure.

        Utils are generated as functions in separate files with filename and function name matching.
        :return:
        """
        utils_dir_path = self._generate_python_module_(self._business_dir_path_, 'utils')

        for util in self._description_.utils:
            with open(utils_dir_path.joinpath(f'{util}.py'), 'w') as f:
                function_code = f'def {util}(*args, **kwargs):\n' \
                                f'{tokens.IDENT_SPACES}pass\n'
                f.write(function_code)

    def _generate_entity_file_(self, entity_dir_path: pathlib.Path, entity: BusinessEntityDescription):
        entity_cap = entity.name.capitalize()
        with open(entity_dir_path.joinpath(f'{entity.name.lower()}.py'), 'w') as f:
            entity_code = io.StringIO()
            if entity.generates_exceptions:
                exception_names = ', '.join(f"{entity_cap}{exception_name}Exception"
                                            for exception_name in entity.generates_exceptions)
                import_exceptions_code = f'from {self._service_name_}.{entity_cap}.exceptions import {exception_names}\n\n\n'
                entity_code.write(import_exceptions_code)

            class_code = f'class {entity_cap}:\n' \
                         f'{tokens.IDENT_SPACES}def __init__(self):\n' \
                         f'{tokens.IDENT_SPACES * 2}pass\n'

            entity_code.write(class_code)

            f.write(entity_code.getvalue())

    @staticmethod
    def _generate_entity_exceptions_(entity_dir_path: pathlib.Path, entity: BusinessEntityDescription):
        with open(entity_dir_path.joinpath(f'exceptions.py'), 'w') as f:
            exception_classes = [
                ClassTemplate(f'{entity.name.capitalize()}{exception_name}Exception', False)
                for exception_name in entity.generates_exceptions
            ]
            for exception in exception_classes:
                exception.add_builtin_parent_class('Exception')

            exception_classes_code = '\n\n'.join(str(exception_class) for exception_class in exception_classes)

            f.write(exception_classes_code)

    def _generate_tests_(self, entity_dir_path: pathlib.Path, entity: BusinessEntityDescription):
        entity_cap = entity.name.capitalize()
        with open(entity_dir_path.joinpath(f'tests.py'), 'w') as f:
            import_code = io.StringIO()
            import_code.write(f'import unittest\n'
                              f'from {self._service_name_}.{entity_cap}.{entity.name.lower()} import {entity_cap}\n')

            test_case_code = io.StringIO()
            test_case_code.write(f'class {entity.name}TestCase(unittest.TestCase):\n'
                                 f'{tokens.IDENT_SPACES}def setUp(self) -> None:\n'
                                 f'{tokens.IDENT_SPACES * 2}self.entity = {entity_cap}()  # ToDo add init of entity\n')

            if entity.generates_exceptions:
                exception_names = ', '.join(f"{entity_cap}{exception_name}Exception"
                                            for exception_name in entity.generates_exceptions)
                import_code.write(f'from {self._service_name_}.{entity_cap}.exceptions import {exception_names}\n')

                test_case_code.write('\n')
                test_case_code.write('\n'.join(f'{tokens.IDENT_SPACES}def test_{entity.name.lower()}_raises_{exception_name.lower()}(self):\n'
                                               f'{tokens.IDENT_SPACES*2}pass\n'
                                               for exception_name in entity.generates_exceptions))

            f.write(f'{import_code.getvalue()}\n\n{test_case_code.getvalue()}')

    def generate_business_entities(self) -> None:
        """
        Generates templates for business entities.

        :return:
        """
        for entity in self._description_.entities:
            entity_cap = entity.name.capitalize()
            entity_dir_path = self._generate_python_module_(self._business_dir_path_, entity_cap)

            self._generate_entity_file_(entity_dir_path, entity)

            if entity.generates_exceptions:
                self._generate_entity_exceptions_(entity_dir_path, entity)

            self._generate_tests_(entity_dir_path, entity)
