import dataclasses


@dataclasses.dataclass(slots=True)
class ParameterTemplate:
    """
    Class for method|function parameter
    """
    name: str
    datatype: str | None
    default: str = ''
    # required: bool = True  ToDo think about version

    def __str__(self):
        return f'{self.name}' \
               f'{f":{self.datatype}" if self.datatype else ""}' \
               f'{f" = {self.default}" if self.default else ""}'
