import dataclasses
from MicroGenerator.Templates.MethodTemplate import MethodTemplate


@dataclasses.dataclass(slots=True)
class FieldTemplate:
    name: str
    datatype: str | None
    is_private: bool = False
    pass_in_constructor: bool = True
    init_method: MethodTemplate | None = None

    def correct_name(self):
        return f'{"_" if self.is_private else ""}{self.name}{"_" if self.is_private else ""}'
