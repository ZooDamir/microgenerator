import dataclasses
import io
from MicroGenerator.Templates.ParameterTemplate import ParameterTemplate
import MicroGenerator.tokens as tokens


@dataclasses.dataclass(slots=True)
class MethodTemplate:
    name: str
    is_async: bool
    parameters: list[ParameterTemplate]
    body_template: list[str]
    # is_static: bool  ToDo later
    # is_private: bool  ToDo later

    def declaration(self):
        builder = io.StringIO()
        if self.is_async:
            builder.write(f'{tokens.IDENT_SPACES}async ')
        builder.write('def (self')
        if self.parameters:
            self.parameters.sort(key=lambda x: x.default)
            builder.write(', ')
            builder.write(', '.join(str(x) for x in self.parameters))
        builder.write('):\n')

        builder.write(''.join(f'{tokens.IDENT_SPACES*2}{line}\n' for line in self.body_template))

        return builder.getvalue()

    def usage(self, params_in_order: list[str]):
        passed_params = ''
        if self.parameters:
            passed_params += ', '
            passed_params += ', '.join(params_in_order)
        return f'{"await " if self.is_async else ""}self.{self.name}(self{passed_params})\n'  # ToDo pass parameters
