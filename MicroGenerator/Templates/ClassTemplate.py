import typing
import io
import MicroGenerator.tokens as tokens
from MicroGenerator.Templates.MethodTemplate import MethodTemplate
from MicroGenerator.Templates.FieldTemplate import FieldTemplate


class ClassTemplate:
    def __init__(self, class_name: str, is_dataclass: bool, py_version: int = 9):
        self.name = class_name
        self._is_dataclass_ = is_dataclass
        self._py_version_ = py_version
        self._parent_classes_ = []
        self._methods_ = []
        self._fields_ = []

    def add_method(self, method_template: MethodTemplate):
        self._methods_.append(method_template)

    def add_custom_parent_class(self, parent_class: typing.Self):
        self._parent_classes_.append(parent_class)

    def add_builtin_parent_class(self, class_name: str):
        self._parent_classes_.append(ClassTemplate(class_name, False))

    def _generate_dataclass_version_(self) -> (io.StringIO, io.StringIO):
        decorator_code = io.StringIO()
        init_code = io.StringIO()
        decorator_code.write(f'@dataclasses.dataclass({"slots=True" if self._py_version_ >= 10 else ""})\n')
        init_code.write("\n".join(f"{f.name}: {f.datatype}" for f in self._fields_))
        if init_code.getvalue() == '':
            init_code.write(f'{tokens.IDENT_SPACES}pass\n')

        return decorator_code, init_code

    def add_field(self, field: FieldTemplate) -> None:
        self._fields_.append(field)

    def _generate_custom_version_(self) -> (io.StringIO, io.StringIO):
        decorator_code = io.StringIO()
        init_code = io.StringIO()
        field_names_sep_by_comma = ', '.join(f'{f.name}: {f.datatype}' for f in self._fields_ if f.pass_in_constructor)
        init_code.write(f'{tokens.IDENT_SPACES}def __init__(self')
        if field_names_sep_by_comma != '':
            init_code.write(f', {field_names_sep_by_comma}):\n')
            for field in self._fields_:
                field_init = '...'
                if field.pass_in_constructor:
                    field_init = field.correct_name()
                elif field.init_method:
                    field_init = field.init_method.usage(['self._conn_string_'])  # ToDo fix
                field_assignment = f'{tokens.IDENT_SPACES*2}self.{field.correct_name()} = {field_init}\n'
                init_code.write(field_assignment)
        else:
            init_code.write('):\n')
            init_code.write(f'{tokens.IDENT_SPACES * 2}pass\n')

        return decorator_code, init_code

    def __str__(self) -> str:
        if self._parent_classes_:
            parent_classes_str = f'({",".join(pc.name for pc in self._parent_classes_)})'
        else:
            parent_classes_str = ''

        if self._is_dataclass_:
            decorator_code, init_code = self._generate_dataclass_version_()
        else:
            decorator_code, init_code = self._generate_custom_version_()

        methods_code = f'{tokens.IDENT_SPACES}\n'.join(method.declaration() for method in self._methods_)

        output = f'{decorator_code.getvalue()}' \
                 f'class {self.name}{parent_classes_str}:\n' \
                 f'{init_code.getvalue()}' \
                 f'{methods_code}'

        return output
