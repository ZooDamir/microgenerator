import abc
import os
import pathlib


class PartGenerator(abc.ABC):
    @staticmethod
    def _generate_python_module_(base_dir: pathlib.Path, module_name: str) -> pathlib.Path:
        module_dir_path = base_dir.joinpath(module_name)
        os.mkdir(module_dir_path)
        open(module_dir_path.joinpath('__init__.py'), 'w').close()

        return module_dir_path
