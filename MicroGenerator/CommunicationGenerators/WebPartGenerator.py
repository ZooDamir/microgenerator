import pathlib
import MicroGenerator.tokens as tokens
from service_description_model import WebDescription
from MicroGenerator.PartGenerator import PartGenerator
from MicroGenerator.Templates.ClassTemplate import ClassTemplate
from MicroGenerator.Templates.FieldTemplate import FieldTemplate
from MicroGenerator.Templates.MethodTemplate import MethodTemplate
from MicroGenerator.Templates.ParameterTemplate import ParameterTemplate


class WebPartGenerator(PartGenerator):
    def __init__(self, base_dir: pathlib.Path, service_name: str, web_description: WebDescription, py_version: int):
        self._web_dir_path_ = self._generate_python_module_(base_dir, 'Web')
        self._py_version_ = py_version
        self._service_name_ = service_name
        self._description_ = web_description

    def _get_optional_output_signature_(self, type_name: str) -> str:
        if self._py_version_ < 10:
            return f'typing.Optional[{type_name}]'
        else:
            return f'{type_name} | None'

    def _generate_web_model_(self, app_dir_path: pathlib.Path, entity_name: str) -> None:
        """
        Generates model and model accessor for entity in web scope of service.

        :param app_dir_path: Path to the base app dir.
        :param entity_name: Name of business entity.
        :return:
        """
        with open(app_dir_path.joinpath('model.py'), 'w') as f:
            service_name = self._service_name_
            import_code = f'import dataclasses\n' \
                          f'{"import typing" if self._py_version_ < 10 else ""}\n' \
                          f'from Web.{service_name}.models import DataAccessor\n'
            model_name = f'{entity_name.capitalize()}Model'
            model_class = ClassTemplate(model_name, True)
            entity_name_lower = entity_name.lower()

            convert_row_to_model_method = f'{tokens.IDENT_SPACES}@staticmethod\n' \
                                          f'{tokens.IDENT_SPACES}def _convert_row_to_model_(row_to_convert_from: tuple) -> {model_name}:\n' \
                                          f'{tokens.IDENT_SPACES*2}"""\n' \
                                          f'{tokens.IDENT_SPACES*2}Converts selected db row to model object.\n' \
                                          f'{tokens.IDENT_SPACES*2}"""\n' \
                                          f'{tokens.IDENT_SPACES*2}# ToDo implement your conversion logic here\n' \
                                          f'{tokens.IDENT_SPACES*2}# output = {model_name}(*row_to_convert_from)\n\n' \
                                          f'{tokens.IDENT_SPACES*2}# return output\n' \
                                          f'{tokens.IDENT_SPACES*2}pass\n'
            get_by_id_method = f'{tokens.IDENT_SPACES}def get_{entity_name_lower}_by_id(self, {entity_name_lower}_id) -> {self._get_optional_output_signature_(model_name)}:\n' \
                               f'{tokens.IDENT_SPACES*2}query = """SELECT ... FROM ... WHERE ... = ?;"""\n' \
                               f'{tokens.IDENT_SPACES*2}cursor = self._select_connection_.cursor()\n' \
                               f'{tokens.IDENT_SPACES*2}data = cursor.execute(query, ({entity_name_lower}_id, )).fetchone()\n' \
                               f'{tokens.IDENT_SPACES*2}cursor.close()\n' \
                               f'{tokens.IDENT_SPACES*2}output = None\n\n' \
                               f'{tokens.IDENT_SPACES*2}if data:\n' \
                               f'{tokens.IDENT_SPACES*3}output = self._convert_row_to_model_(data)\n' \
                               f'{tokens.IDENT_SPACES*2}return output\n'
            get_all_method = f'{tokens.IDENT_SPACES}def get_all_{entity_name_lower}s(self) -> list[{model_name}]:\n' \
                             f'{tokens.IDENT_SPACES*2}query = """SELECT ... FROM ...;"""\n' \
                             f'{tokens.IDENT_SPACES*2}cursor = self._select_connection_.cursor()\n' \
                             f'{tokens.IDENT_SPACES*2}data = cursor.execute(query).fetchall()\n' \
                             f'{tokens.IDENT_SPACES*2}cursor.close()\n' \
                             f'{tokens.IDENT_SPACES*2}output = []\n\n' \
                             f'{tokens.IDENT_SPACES*2}if data:\n' \
                             f'{tokens.IDENT_SPACES*3}output = [self._convert_row_to_model_(row) for row in data]\n\n' \
                             f'{tokens.IDENT_SPACES*2}return output\n'
            delete_entity_method = f'{tokens.IDENT_SPACES}def delete_{entity_name_lower}(self, {entity_name_lower}_id):\n' \
                                   f'{tokens.IDENT_SPACES*2}query = """DELETE FROM ... WHERE ... = ?;"""\n' \
                                   f'{tokens.IDENT_SPACES*2}cursor = self._delete_connection_.cursor()\n' \
                                   f'{tokens.IDENT_SPACES*2}cursor.execute(query, ({entity_name_lower}_id, ))\n' \
                                   f'{tokens.IDENT_SPACES*2}self._delete_connection_.commit()\n' \
                                   f'{tokens.IDENT_SPACES*2}cursor.close()\n\n' \
                                   f'{tokens.IDENT_SPACES*2}return\n'
            insert_entity_method = f'{tokens.IDENT_SPACES}def insert_{entity_name_lower}(self, {entity_name_lower}):\n' \
                                   f'{tokens.IDENT_SPACES*2}# Generating query that can work only in simple cases, so consider rewriting it. As a bonus you get a bit of performance\n' \
                                   f'{tokens.IDENT_SPACES*2}values_to_insert = dataclasses.astuple({entity_name_lower})\n' \
                                   f'{tokens.IDENT_SPACES*2}param_subs = ("?, " * len(values_to_insert))[:-2]\n' \
                                   f'{tokens.IDENT_SPACES * 2}query = f"""INSERT INTO ... VALUES ({tokens.CURL_OPEN}param_subs{tokens.CURL_CLOSE});"""\n' \
                                   f'{tokens.IDENT_SPACES * 2}cursor = self._insert_connection_.cursor()\n' \
                                   f'{tokens.IDENT_SPACES * 2}cursor.execute(query, values_to_insert)\n' \
                                   f'{tokens.IDENT_SPACES*2}self._insert_connection_ .commit()\n' \
                                   f'{tokens.IDENT_SPACES * 2}cursor.close()\n\n' \
                                   f'{tokens.IDENT_SPACES * 2}return\n'
            model_accessor_code = f'class {entity_name.capitalize()}Accessor(DataAccessor):\n' \
                                  f'{convert_row_to_model_method}\n' \
                                  f'{get_by_id_method}\n' \
                                  f'{get_all_method}\n' \
                                  f'{delete_entity_method}\n' \
                                  f'{insert_entity_method}' \

            f.write(f'{import_code}\n\n{str(model_class)}\n\n{model_accessor_code}')

    @staticmethod
    def _generate_web_view_(app_dir_path: pathlib.Path, entity_name: str):
        with open(app_dir_path.joinpath('views.py'), 'w') as f:
            import_code = 'import asyncio\n' \
                          'from aiohttp.web import View\n'

            method_body_example = f'{tokens.IDENT_SPACES*2}"""\n' \
                                  f'{tokens.IDENT_SPACES*2}\n' \
                                  f'{tokens.IDENT_SPACES*2}"""\n' \
                                  f'{tokens.IDENT_SPACES*2}# body = await self.request.json()\n' \
                                  f'{tokens.IDENT_SPACES*2}# response = web.json_response({tokens.CURL_OPEN}"hello": "world"{tokens.CURL_CLOSE}, status=200)\n' \
                                  f'{tokens.IDENT_SPACES*2}# return response\n' \
                                  f'{tokens.IDENT_SPACES*2}pass\n'

            single_entity_view_code = f'class {entity_name.capitalize()}View(View):\n' \
                                      f'{tokens.IDENT_SPACES}async def get(self):\n' \
                                      f'{method_body_example}' \
                                      f'\n' \
                                      f'{tokens.IDENT_SPACES}async def post(self):\n' \
                                      f'{method_body_example}' \
                                      f'\n' \
                                      f'{tokens.IDENT_SPACES}async def delete(self):\n' \
                                      f'{method_body_example}'

            multiple_entity_view_code = f'class {entity_name.capitalize()}sView(View):\n' \
                                        f'{tokens.IDENT_SPACES}async def get(self):\n' \
                                        f'{method_body_example}' \
                                        f'\n' \
                                        f'{tokens.IDENT_SPACES}async def post(self):\n' \
                                        f'{method_body_example}' \
                                        f'\n' \
                                        f'{tokens.IDENT_SPACES}async def delete(self):\n' \
                                        f'{method_body_example}'

            f.write(f'{import_code}\n\n{single_entity_view_code}\n\n{multiple_entity_view_code}')

    def _generate_project_app_urls_(self, app_dir_path: pathlib.Path) -> None:
        with open(app_dir_path.joinpath('urls.py'), 'w') as f:
            other_urls_import = "".join(f"import Web.{app.business_entity.capitalize()}App.urls{tokens.NEW_LINE}"
                                        for app in self._description_.apps_description
                                        if app.generate_view)
            import_urls_code = f'from aiohttp.web import View\n' \
                               f'{other_urls_import}'

            add_with_prefix_function = f'def add_with_prefix(prefix: str, urls_to_add: list[(str, View)], all_urls: list[(str, View)]):\n' \
                                       f'{tokens.IDENT_SPACES}all_urls.extend(((prefix+x[0], x[1]) for x in urls_to_add))\n'

            populate_urls_part = '\n'.join(f"add_with_prefix('/{app.business_entity.lower()}s', Web.{app.business_entity.capitalize()}App.urls.urls, urls)"
                                           for app in self._description_.apps_description
                                           if app.generate_view)

            f.write(f'{import_urls_code}\n\n'
                    f'urls = []\n\n\n'
                    f'{add_with_prefix_function}\n\n'
                    f'{populate_urls_part}\n')

    def _generate_project_app_models_(self, app_dir_path: pathlib.Path) -> None:
        with open(app_dir_path.joinpath('models.py'), 'w') as f:
            import_code = 'import abc\n' \
                          'import asyncio\n' \
                          'import sqlite3\n'
            data_accessor_class = ClassTemplate('DataAccessor', False, self._py_version_)
            data_accessor_class.add_field(FieldTemplate('max_connections', 'int', True))
            data_accessor_class.add_field(FieldTemplate('conn_string', 'str', True))
            conn_queue_init_method = MethodTemplate('fill_conn_queue', True,
                                                    [ParameterTemplate('conn_string', 'str')],
                                                    [
                                                        'self._conn_queue_ = asyncio.Queue(max_size=self._max_connections_)',  # ToDo add notion of fields and pass them
                                                        '',
                                                        'for _ in range(self._max_connections_):',
                                                        ''
                                                        f'{tokens.IDENT_SPACES}self._conn_queue_.put_nowait(sqlite3.connect(conn_string))'
                                                    ])
            data_accessor_class.add_method(conn_queue_init_method)
            data_accessor_class.add_field(FieldTemplate('conn_queue', None, True, False, conn_queue_init_method))
            data_accessor_class.add_builtin_parent_class('abc.ABC')
            print(str(data_accessor_class))
            data_accessor_code = f'class DataAccessor(abc.ABC):\n' \
                                 f'{tokens.IDENT_SPACES}def __init__(self, db_path: str):\n' \
                                 f'{tokens.IDENT_SPACES*2}self._select_connection_ = sqlite3.connect(db_path)\n' \
                                 f'{tokens.IDENT_SPACES*2}self._insert_connection_ = sqlite3.connect(db_path)\n' \
                                 f'{tokens.IDENT_SPACES*2}self._delete_connection_ = sqlite3.connect(db_path)\n' \
                                 f'{tokens.IDENT_SPACES*2}self._update_connection_ = sqlite3.connect(db_path)\n'

            f.write(f'{import_code}\n\n{data_accessor_code}')

    def _generate_project_app_builder_(self, app_dir_path: pathlib.Path) -> None:
        with open(app_dir_path.joinpath(f'{self._service_name_}Builder.py'), 'w') as f:
            accessors_import = "".join(f'from Web.{app.business_entity.capitalize()}App.model import {app.business_entity.capitalize()}Accessor\n'
                                       for app in self._description_.apps_description
                                       if app.generate_model)
            import_code = f'import aiohttp.web as web\n' \
                          f'{accessors_import}'

            accessors_init_code = ''.join(f"{tokens.IDENT_SPACES*2}self._app_['{app.business_entity.lower()}_accessor'] = {app.business_entity.capitalize()}Accessor(self._db_path_)\n"
                                          for app in self._description_.apps_description
                                          if app.generate_model)

            # ToDo add configuring service from settings
            init_code = f"{tokens.IDENT_SPACES}def __init__(self):\n" \
                        f"{tokens.IDENT_SPACES*2}self._app_ = web.Application()\n" \
                        f"{tokens.IDENT_SPACES*2}self._db_path_ = 'SOME PATH'\n"
            add_routes_code = f'{tokens.IDENT_SPACES}def add_routes(self, routes: [str, web.View]):\n' \
                              f'{tokens.IDENT_SPACES*2}for path, view in routes:\n' \
                              f'{tokens.IDENT_SPACES*3}self._app_.router.add_view(path, view)\n'
            add_startup_handler_code = f'{tokens.IDENT_SPACES}def add_startup_handler(self, handler):\n' \
                                       f'{tokens.IDENT_SPACES*2}self._app_.on_startup.append(handler)\n'
            get_app_code = f'{tokens.IDENT_SPACES}def get_app(self):\n' \
                           f'{accessors_init_code}\n' \
                           f'{tokens.IDENT_SPACES*2}return self._app_\n'
            app_builder_code = f'class {self._service_name_}AppBuilder:\n' \
                               f'{init_code}\n' \
                               f'{add_routes_code}\n' \
                               f'{add_startup_handler_code}\n' \
                               f'{get_app_code}'

            f.write(f'{import_code}\n\n{app_builder_code}')

    def generate_web_main(self) -> None:
        with open(self._web_dir_path_.joinpath('main.py'), 'w') as f:
            service_name = self._service_name_
            import_code = f'import aiohttp.web as web\n' \
                          f'from Web.{service_name}.{service_name}Builder import {service_name}AppBuilder\n' \
                          f'from Web.{service_name}.urls import urls\n'
            builder_code = f'builder = {service_name}AppBuilder()\n' \
                           f'builder.add_routes(urls)\n'
            web_run_code = f'app = builder.get_app()\n' \
                           f'web.run_app(app, port=8080)\n'

            f.write(f'{import_code}\n\n{builder_code}\n{web_run_code}')

    def generate_project_app(self) -> None:
        app_dir_path = self._generate_python_module_(self._web_dir_path_, f'{self._service_name_}')
        self._generate_project_app_urls_(app_dir_path)
        self._generate_project_app_models_(app_dir_path)
        self._generate_project_app_builder_(app_dir_path)

    @staticmethod
    def _generate_web_app_urls_(app_dir_path: pathlib.Path, entity: str) -> None:
        with open(app_dir_path.joinpath('urls.py'), 'w') as f:
            cap_entity = entity.capitalize()
            import_views_code = f'from Web.{cap_entity}App.views import {cap_entity}View, {cap_entity}sView'
            multiple_view = f"('/', {cap_entity}sView)"
            single_view = f"('/{tokens.CURL_OPEN}{entity.lower()}_id{tokens.CURL_CLOSE}/', {cap_entity}View)"

            app_urls_code = f"urls = [\n" \
                            f"{tokens.IDENT_SPACES}{multiple_view},\n" \
                            f"{tokens.IDENT_SPACES}{single_view},\n" \
                            f"]\n"

            f.write(f'{import_views_code}\n\n{app_urls_code}')

    @staticmethod
    def _generate_app_tests_(app_dir_path: pathlib.Path, app_name: str) -> None:
        with open(app_dir_path.joinpath(f'tests.py'), 'w') as f:
            import_code = f'import unittest\n'

            test_case_code = f'class {app_name.capitalize()}AppTestCase(unittest.TestCase):\n' \
                             f'{tokens.IDENT_SPACES}def setUp(self) -> None:\n' \
                             f'{tokens.IDENT_SPACES * 2}pass\n'

            f.write(f'{import_code}\n\n{test_case_code}')

    def generate_web_apps(self) -> None:
        """
        Generates web apps.

        :param web_dir_path: Path to the base web dir.
        :return:
        """

        for app in self._description_.apps_description:
            app_dir_path = self._generate_python_module_(self._web_dir_path_, f'{app.business_entity}App')

            if app.generate_model:
                self._generate_web_model_(app_dir_path, app.business_entity)

            if app.generate_view:
                self._generate_web_view_(app_dir_path, app.business_entity)
                self._generate_web_app_urls_(app_dir_path, app.business_entity)

            self._generate_app_tests_(app_dir_path, app.business_entity)
