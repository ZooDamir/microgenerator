# MicroGenerator

## Motivation

This project appeared from the need to write a lot of boilerplate code for different microservices.

It aims to remove setup time for creating new microservice.

## Features

1. Generating of service structure. Including separation of business logic and web communication.
2. Support for different versions of Python. For now, it's just simple slotted dataclasses for Python >= 3.10
3. Generation of template code in web part of service.

Now I need to implement proper reading of description.
Another thing can be generating additional views according to user specs.

It will be cool to generate communication with other services.

It is based on Aiohttp framework. Probably, add pure sockets and MQ as well.

As a guide for dev experience I'll stick with django manage.py


## ToDo
1. ~~Tests in web part~~
2. ~~Tests for exceptions in business part~~
3. Background tasks for web app
4. ~~Generate better readme~~
5. Generate API description for apps
6. Add datatypes class
7. Add notion of necessary imports not to hardcode them
8. Add context for class creation to store necessary imports and probably smth else
9. Refactor forever...
10. Reduce number of db connections
11. Add exception handling templates
12. Add CLI/Web interface
13. Add common templates as @login_required
14. *Add other communications (Rabbit/Kafka)
